


const v_modalMenu = document.querySelector('.modal-menu'),
v_headerMenuM = document.querySelector('.header-menu-m'),
v_close = document.querySelectorAll('.close');


if (v_headerMenuM) {
    v_headerMenuM.addEventListener('click', e => {
        e.preventDefault();
        v_modalMenu.classList.add('active');
    })
    v_close.forEach(item => {
        item.addEventListener('click', e => {
            e.preventDefault();
            v_modalMenu.classList.remove('active');
        })
    })
}